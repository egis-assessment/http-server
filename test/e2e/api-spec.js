var chakram = require('chakram'),
    expect = chakram.expect;
const { port } = require('../../_helpers/config');

describe('Web server', function() {

    let response;
    before(function() {
        response = chakram.get(`http://localhost:${port}`);
    })

    it('should return status code 201', function() {
        expect(response).to.have.status(201);
        return chakram.wait();
    });

    it('should return "Hello world" as the response', function() {
        var body = {
            'response': 'hello world'
        };
        expect(response).to.comprise.of.json(body);
        return chakram.wait();
    });
});