const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('./_helpers/error-handler');
const { port } = require('./_helpers/config');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// api routes
app.all('*', function(req, res, next) {
    res.status(201).send({ 'response': 'hello world' });
});

// global error handler
app.use(errorHandler);

// start server
const server = app.listen(port, function() {
    console.log('Server listening on port ' + port);
});